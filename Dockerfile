FROM openjdk:8-alpine
LABEL maintainer="Sergio Rivas Medina -> sergiorm96@gmail.com"

ADD ./target/ /app
EXPOSE 9006
WORKDIR /app
ENTRYPOINT ["java", "-jar", "bff-seller-0.0.1-SNAPSHOT.jar"]
