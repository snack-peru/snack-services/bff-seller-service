package com.snack.services;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.google.gson.Gson;
import com.snack.models.User;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private static String streamToString(InputStream inputStream) {
        String text = new Scanner(inputStream, "UTF-8").useDelimiter("\\Z").next();
        return text;
    }

    
    public List<User> getAllUsers() {
        String jsonString = null;
        Gson g = new Gson();

        try {
            URL url = new URL("http://13.58.62.62:9001/users");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("charset", "utf-8");
            connection.connect();

            InputStream inStream = connection.getInputStream();
            jsonString = streamToString(inStream);

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        List<User> userList = new ArrayList<User>();
        JSONArray jsonarray = new JSONArray(jsonString);
        for (int i = 0; i < jsonarray.length(); i++) {
            JSONObject jsonObject = jsonarray.getJSONObject(i);
            User user = g.fromJson(jsonObject.toString(), User.class);
            userList.add(user);
        }

        return userList;
    }
}